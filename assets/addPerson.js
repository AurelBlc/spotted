const $ = require("jquery");

var row = document.getElementById("form_row");

var addLineButton = document.getElementById("addLine");

addLineButton.addEventListener("click", (e)=> {
  var divForm = document.createElement("div");
  divForm.classList.add("form-inputs");
  divForm.innerHTML = `
    <input type="text" class="form-control mr-2" id="name" name="name" placeholder="Nom" required>
    <input type="text" class="form-control mr-2" id="surname" name="surname" placeholder="Prénom" required>
    <input type="text" class="form-control mr-2" id="phone" name="phone" placeholder="Téléphone" required>
  `;

  row.appendChild(divForm);
})

/* Récuparation de l'url et de la date */
var options = document.querySelectorAll('option');
var choice; 
options.forEach( (option) => {
  
  if(option.getAttribute('selected')!== null){
    choice = option.value;
  }
})

var theForm = document.getElementById("addPersonForm");

theForm.addEventListener("submit", (e) =>{
  e.preventDefault();
  
  var count_data = 0;

  var names = theForm.querySelectorAll("#name");
  var surnames = theForm.querySelectorAll("#surname");
  var phones = theForm.querySelectorAll("#phone");
  count_data = names.length;
  var person = {};
  var listPerson = [];

  if(count_data > 0){

    for (let index = 0; index < count_data; index++) {

      person = {
        "name" : names[index].value,
        "surname": surnames[index].value,
        "phone": phones[index].value
      };
      listPerson.push(person);   
    }
    
    // On crée un objet XMLHttpRequest
    var xhr = new XMLHttpRequest();

    // On initialise la requête avec open()
    xhr.open("POST", "/addPerson" + choice);

    // On récupère la réponse au format json
    xhr.responseType = "json";

    // envoie de la requête
    xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xhr.send(listPerson);

    xhr.onload = function () {
      //Si le statut HTTP n'est pas 200...
      if (xhr.status != 200) {
        //...On affiche le statut et le message correspondant
        alert("Erreur " + xhr.status + " : " + xhr.statusText);
        //Si le statut HTTP est 200, on affiche la réponse
      } else {
        var json = JSON.stringify(xhr.response);
        var obj = JSON.parse(json);
      }
    }
  }
  return false;
});