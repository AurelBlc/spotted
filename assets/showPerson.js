const $ = require("jquery");
const tbody = document.getElementById("tbody-person-show");

var select = document.getElementById("dateChoice");
select.addEventListener("change", function (e) {
  var choice = select.value;

  document.getElementsByTagName("option").forEach(function (opt) {
    // On enlève le selected! en fonction
    if (opt.getAttribute["selected"] != "undefined") {
      opt.removeAttribute("selected");
    }

    // et on l'applique à l'option choisie
    if (opt.value == choice) {
      opt.setAttribute("selected", "");
    }
  });

  // // Modification de la value du bouton d'add
  // let buttonAdd = document.getElementById("addPersonForm");
  // buttonAdd.setAttribute("action", "/ets/addPerson" + choice);

  // On crée un objet XMLHttpRequest
  var xhr = new XMLHttpRequest();

  // On initialise la requête avec open()

  xhr.open("GET", "/showList" + e.target.value);

  // On récupère la réponse au format json
  xhr.responseType = "json";

  // envoie de la requête
  xhr.send();

  xhr.onload = function () {
    //Si le statut HTTP n'est pas 200...
    if (xhr.status != 200) {
      //...On affiche le statut et le message correspondant
      alert("Erreur " + xhr.status + " : " + xhr.statusText);
      //Si le statut HTTP est 200, on affiche la réponse
    } else {
      var json = JSON.stringify(xhr.response);
      var obj = JSON.parse(json);
      var newList = obj["list"];

      // On efface le contenu du tableau
      var list_TR = document.getElementsByClassName("tr-js");
      var lenght = list_TR.length;
      while (lenght != 0) {
        // bug : supprime 1 élément sur 2...
        // on test que la liste est bien vide
        list_TR = document.getElementsByClassName("tr-js");
        lenght = list_TR.length;

        list_TR.forEach(function (tr) {
          tbody.removeChild(tr);
        });
      }

      // On ajoute le nouveau contenu
      newList.forEach(function (person) {
        var tr = document.createElement("tr");
        tr.setAttribute("class", "tr-js");
        var date = person.dateIn;
        var H = date.substring(11, 13);
        var m = date.substring(14, 16);

        var html = "<td>" + person.name + "</td>";
        html += "<td>" + person.surname + "</td>";
        html += "<td>" + person.phoneNumber + "</td>";
        html += "<td align='center'>" + H + ":" + m + "</td>";
        if (person.isSetManualy) {
          html += "<td align='center'><i class=\"fas fa-user-edit\"></i></td>";
        } else {
          html += "<td align='center'><i class=\"fas fa-qrcode\"></i></td>";
        }

        tr.innerHTML = html;
        tbody.append(tr);
      });

      // Modification du lien pour DL le pdf
      var dl = document.getElementById("DL");
      // On régénère l'url souhaitée
      dl.setAttribute("action", "/ets/createPdf" + choice);
    }
  };

  //Si la requête n'a pas pu aboutir...
  xhr.onerror = function () {
    alert("La requête a échoué");
  };
});
