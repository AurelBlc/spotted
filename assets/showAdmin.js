const $ = require("jquery");

function clear(_dom) {
  _dom.innerHTML = "";
}

$(document).ready(function () {
  $("#myInput").on("keyup", function () {
    var value = $(this).val().toLowerCase();
    $("#myTable div").filter(function () {
      let table = document.getElementById("myTable");
      let buttons = table.querySelectorAll(".btn.btn-primary.btn-show-admin");
      buttons.forEach(function (button) {
        button.style.display = "inline-block";
      });
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1);
    });
  });
});

let display = document.getElementById("display");

let buttons = document.querySelectorAll(".btn-show-admin");

buttons.forEach((button) => {
  button.addEventListener("click", function (e) {
    // On crée un objet XMLHttpRequest
    var xhr = new XMLHttpRequest();

    // On initialise la requête avec open()
    xhr.open("GET", "/show/su/" + e.target.id);

    // On récupère la réponse au format json
    xhr.responseType = "json";

    // envoie de la requête
    xhr.send();

    xhr.onload = function () {
      //Si le statut HTTP n'est pas 200...
      if (xhr.status != 200) {
        //...On affiche le statut et le message correspondant
        alert("Erreur " + xhr.status + " : " + xhr.statusText);
        //Si le statut HTTP est 200, on affiche la réponse
      } else {
        var json = JSON.stringify(xhr.response);
        var obj = JSON.parse(json);
        var admin = obj.admin;
        var etsList = obj.etsList;

        var display = document.getElementById("display");
        clear(display);

        /* Construction de la modal */

        /* Création des éléments de la modal */
        var modalDialog = document.createElement("div");
        modalDialog.classList = "modal-dialog";
        modalDialog.setAttribute("role", "document");

        var modalContent = document.createElement("div");
        modalContent.classList = "modal-content";

        var modalHeader = document.createElement("div");
        modalHeader.classList = "modal-header";

        var modalTitle = document.createElement("h5");
        modalTitle.classList = "modal-title";
        modalTitle.id = "modalLabel";
        modalTitle.innerHTML = admin.name + " " + admin.surname;

        var imgModalTitle = document.createElement("img");
        imgModalTitle.style.maxWidth = "50px";
        if (admin.filename != "empty") {
          imgModalTitle.setAttribute("src", "/images/admin/" + admin.filename);
        } else {
          imgModalTitle.setAttribute("src", "/images/admin/empty.png");
        }

        var button = document.createElement("button");
        button.setAttribute("type", "button");
        button.setAttribute("data-dismiss", "modal");
        button.setAttribute("aria-label", "Close");
        button.classList = "close";

        var span = document.createElement("span");
        span.setAttribute("aria-hidden", true);
        span.innerHTML = "&times;";

        /* div à remplir d'infos */
        var modalBody = document.createElement("div");
        modalBody.classList = "modal-body";

        var modalContainer = new ModalContainer();
        var content = modalContainer.create(etsList);
        modalBody.appendChild(content);

        var modalFooter = document.createElement("div");
        modalFooter.classList = "modal-footer";

        var buttonClose = document.createElement("button");
        buttonClose.classList = "btn btn-secondary";
        buttonClose.setAttribute("data-dismiss", "modal");
        buttonClose.innerHTML = "Fermer";

        /* Création de la modal */
        button.appendChild(span);
        modalHeader.appendChild(imgModalTitle);
        modalHeader.appendChild(modalTitle);
        modalHeader.appendChild(button);

        modalFooter.appendChild(buttonClose);

        modalContent.appendChild(modalHeader);
        modalContent.appendChild(modalBody);
        modalContent.appendChild(modalFooter);

        modalDialog.appendChild(modalContent);

        display.appendChild(modalDialog);

        /* Ajout des event listener pour fermer la fenêtre */
        button.addEventListener("click", function (e) {
          display.innerHTML = "";
          display.display = "none";
        });

        buttonClose.addEventListener("click", function (e) {
          display.innerHTML = "";
          display.display = "none";
        });
      }
    };
    //Si la requête n'a pas pu aboutir...
    xhr.onerror = function () {
      alert("La requête a échoué");
    };
  });
});

export class ModalContainer {
  constructor() {}

  create(_ets) {
    var container = document.createElement("div");
    container.classList = "container";

    var rowEts = document.createElement("div");
    rowEts.classList = "row";

    _ets.forEach(function (ets) {
      var block = document.createElement("div");
      block.classList = "col col-lg-6 col-md-6 col-sm-12";

      var blockRowImg = document.createElement("div");
      blockRowImg.classList = "row justify-content-center m-3";
      var blockImg = document.createElement("img");
      blockImg.id = ets.url;
      if (ets.filename != "empty") {
        blockImg.setAttribute("src", "/images/ets/" + ets.filename);
      } else {
        blockImg.setAttribute("src", "/images/ets/empty.png");
      }
      blockImg.style.maxWidth = "80px";

      var blockRowTitle = document.createElement("h5");
      blockRowTitle.classList = "row justify-content-center";
      blockRowTitle.innerText = ets.name;

      var blockRowButton = document.createElement("div");
      blockRowButton.classList = "row justify-content-center";
      var blockButtonSearch = document.createElement("a");
      blockButtonSearch.href = "/ets/show/" + ets.url;
      blockButtonSearch.classList = "btn btn-secondary mr-2";
      blockButtonSearch.innerHTML = '<i class="fa fa-search"></i>';

      var blockButtonList = document.createElement("a");
      blockButtonList.href = "/listPerson/" + ets.url;
      blockButtonList.classList = "btn btn-success mr-2";
      blockButtonList.innerHTML = '<i class="fa fa-users"></i>';

      blockRowImg.appendChild(blockImg);
      blockRowButton.appendChild(blockButtonSearch);
      blockRowButton.appendChild(blockButtonList);

      block.appendChild(blockRowTitle);
      block.appendChild(blockRowImg);
      block.appendChild(blockRowButton);

      container.appendChild(block);
    });

    return container;
  }
}
