<?php

namespace App\Command;

use App\Repository\SpotRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CommandCleanUpSpots extends Command
{
    /**
     * @var SpotRepository
     */
    private $spotRepository;


    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:cleanup';

    public function __construct(SpotRepository $spotRepository)
    {
        $this->spotRepository = $spotRepository;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Suppression des spots de plus de 14 jours')
            ->addOption('dry-run', null, InputOption::VALUE_NONE, 'Dry run');
    }

    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        if ($input->getOption('dry-run')) {
            $io->note('Dry mode enabled');
            $count = $this->spotRepository->countOldSpots();
        } else {
            $count = $this->spotRepository->removeOldSpots();
        }

        $io->success(sprintf('Deleted "%d" old spots.', $count));

        return 0;
    }
}
