<?php

namespace App\Controller\Admin;

use DateTime;
use App\Entity\Ets;
use App\Entity\Spot;
use App\Form\SpotType;
use App\Repository\EtsRepository;
use App\Repository\SpotRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class FormController extends AbstractController
{
    /**
     * @var SpotRepository
     */
    private $spot;

    /**
     * @var ObjectManager
     */
    private $em;

    public function __construct(SpotRepository $spot, EtsRepository $etsRepository, EntityManagerInterface $em, Security $security)
    {
        $this->spot = $spot;
        $this->em = $em;
        $this->security = $security;
        $this->etsRepository = $etsRepository;
    }

    /**
     * @Route("/form/{url}/{date_request}", name="admin.spot.new")
     * @ParamConverter("date_request", options={"format": "d-m-Y"})
     * @param Ets $ets
     * @param Request $request
     * @return Response
     */
    public function new(Ets $ets, Request $request, DateTime $date_request)
    {
        $user = $this->security->getUser();
        $objDate = new DateTime();
        $today = $objDate->format('d-m-Y');
        $addAfter = false;
        /* Check si on essaie pas d'accéder à une ancienne date */
        $today = new DateTime('NOW');
        $diff_dateTime = date_diff($today, $date_request, true);
        $diff = (int)$diff_dateTime->format("%a");

        if ($diff > 14) {
            throw $this->createAccessDeniedException();
        } else {
                    
            $spot = new Spot($date_request);
            $spot->setIdEts($ets->getId());

            if($date_request->format('d-m-Y') !== $today){
                $addAfter = true;
            }

            $form = $this->createForm(SpotType::class, $spot);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                
                if($request->get('isSetManualy')){
                    $spot->setIsSetManualy(true);
                }
                else{
                    $spot->setIsSetManualy(false);
                }

                // $spot->setDateIn($date_request);               
                
                $this->em->persist($spot);
                $this->em->flush();
                $this->addFlash('success', 'Personne ajoutée');

            
                if($user){
                    return $this->redirectToRoute('admin.person.show', [
                        'url' => $ets->getUrl()."/".$date_request->format("d-m-Y")
                    ]);
                }
                else{
                    return $this->redirectToRoute('home');
                }
            }
        }

        return $this->render('form/person.html.twig', [
            'form' => $form->createView(),
            'ets' => $ets,
            'current_menu' => 'ets',
            'admin' => $user,
            'date' => $date_request,
            'addAfter' => $addAfter
        ]);
    }
}
