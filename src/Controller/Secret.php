<?php

namespace App\Controller;

use Defuse\Crypto\Key;
use Defuse\Crypto\Crypto;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class Secret extends AbstractController{
    
    /**
     * Load and return the encrypt key
     *
     * @return Key
     */
    private function loadEncryptionKeyFromConfig()
    {
        $keyAscii = rtrim(file_get_contents("../config/secret/spotted.txt"));
        return Key::loadFromAsciiSafeString($keyAscii);
    }

    /**
     * Return an encrypted string
     *
     * @param string $clearString
     * @return string
     */
    public function encrypt(string $clearString)
    {
        $keyAscii = $this->loadEncryptionKeyFromConfig();
        $crypto = Crypto::encrypt($clearString, $keyAscii);
        return $crypto;
    }

    public function decrypt(string $secret)
    {
        $keyAscii = $this->loadEncryptionKeyFromConfig();

        try {
            
            $uncrypto = Crypto::decrypt($secret, $keyAscii);
            return $uncrypto;
        } catch (\Throwable $th) {
            return $th->getMessage();
        }
    }
    


}