<?php

namespace App\Controller;

use DateTime;
use DatePeriod;
use DateInterval;
use App\Entity\Ets;
use App\Entity\Admin;
use App\Form\EtsType;
use App\Repository\EtsRepository;
use App\Repository\SpotRepository;
use App\Repository\AdminRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Core\User\User;

class EtsController extends AbstractController
{
    /**
     * @var EtsRepository
     */
    private $etsRepository;
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EtsRepository $etsRepository, SpotRepository $spotRepository, AdminRepository $adminRepository,  EntityManagerInterface $em, Security $security)
    {
        $this->etsRepository = $etsRepository;
        $this->spotRepository = $spotRepository;
        $this->adminRepository = $adminRepository;
        $this->em = $em;
        $this->security = $security;
    }

    /**
     * @Route("/ets", name="ets.index")
     * @return Response
     */

    public function index(EtsRepository $etsRepository): Response
    {
        $user = $this->security->getUser();
        $user->setLastConnexionAt(new DateTime());
        $this->em->persist($user);
        $this->em->flush();

        $today = new DateTime();

        if ($user->getUserGroup() == 8) {
            //dd($user);
            $this->redirectToRoute("ets.admin.index");
        }

        $message = '';

        if (!isset($user)) {
            $this->addFlash('error', 'Vous n\'avez pas les autorisations nécessaires');
            return $this->redirectToRoute('home');
        } elseif (isset($user) && ($user->getUserGroup() == 1 || $user->getUserGroup() == 5)) {
            //retourne la liste des établissements liés au compte
            $ets = $this->etsRepository->findBy(
                ['owner_id' => $user->getId()]
            );

            if (empty($ets)) {
                $message = 'Vous n\'avez pas encore créé de salle...';
            }

            return $this->render('ets/index.html.twig', [
                'current_menu' => 'ets',
                'ets' => $ets,
                'message' => $message,
                'admin' => $user,
                'today' => $today
            ]);
        }

        return $this->redirectToRoute($user->getUserGroup() == 8 ? "ets.admin.index" : "ets.index");
    }

    /**
     * @Route("/ets/su", name="ets.admin.index")
     *
     * @param AdminRepository $adminRepository
     * @return Response
     */
    public function indexAdmin(AdminRepository $adminRepository): Response
    {

        $user = $this->security->getUser();
        if (!isset($user)) {
            $this->addFlash('error', 'Vous n\'avez pas les autorisations nécessaires');
            return $this->redirectToRoute('home');
        } else {
            $admins = $this->adminRepository->findAll();
        }

        return $this->render("ets/index.su.html.twig", [
            "admins" => $admins
        ]);
    }

    /**
     * @Route("/ets/edit/{url}", name="admin.ets.edit")
     * @return Response
     */
    public function edit(Ets $ets, Request $request): Response
    {
        $user = $this->security->getUser();
        if (!isset($user)) {
            $this->addFlash('error', 'Vous n\'avez pas les autorisations nécessaires');
            return $this->redirectToRoute('home');
        } elseif (isset($user) && ($user->getUserGroup() == 1 || $user->getUserGroup() == 5 || $user->getUserGroup() == 8)) {
            $form = $this->createForm(EtsType::class, $ets);
            $form->handleRequest($request);
            //dump($form);

            if ($form->isSubmitted() && $form->isValid()) {
                $ets->setUpdatedAt(new DateTime());
                $this->em->persist($ets);
                $this->em->flush();
                $this->addFlash('success', 'Informations modifiées avec succès');
                return $this->redirectToRoute('ets.show', array('url' => $ets->getUrl));
            }

            return $this->render('ets/edit.html.twig', [
                'form' => $form->createView(),
                'ets' => $ets,
                'current_menu' => 'ets',
                'button' => 'Editer',
                'action' => 'edit',
                'admin' => $user
            ]);
        }
    }

    /**
     * Fonction AJAX
     * @Route("show/su/{id}", "su.ets.show")
     *
     * @param Admin $admin
     * @return Response
     */
    public function adminGetsInfo(int $id, EtsRepository $etsRepository, AdminRepository $adminRepository): JsonResponse
    {
        $user = $this->security->getUser();
        $admin = $this->adminRepository->find($id);
        $admin->setPassword("*******************");

        if ($user->getUserGroup() == 8) {

            $etsList = $this->etsRepository->findEtsByAdmin($admin);

            return $this->json([
                'message' => 'success',
                'display' => true,
                'admin' => $admin,
                'etsList' => $etsList
            ], 200);
        } else {
            $this->addFlash('error', 'Vous n\'avez pas les autorisations nécessaires');
            return $this->redirectToRoute('home');
        }
    }

    /**
     * Fonction qui display un graph dans la partie admin ("Affluence")     
     * @Route("/ets/show/{url}", name="admin.ets.show")
     * @return Response
     */
    public function show(Ets $ets, Request $request): Response
    {
        $user = $this->security->getUser();
        if (!isset($user)) {
            $this->addFlash('error', 'Vous n\'avez pas les autorisations nécessaires');
            return $this->redirectToRoute('home');
        } elseif (isset($user) && ($user->getUserGroup() == 1 || $user->getUserGroup() == 5 || $user->getUserGroup() == 8)) {

            $start = (new DateTime())->modify('+1 day');
            $end = (new DateTime)->modify('-14 days');
            $interval = new DateInterval('P1D');
            //Pour obtenir un array de date, il faut passer par la fonction DatePeriod
            $daterange = new DatePeriod($end, $interval, $start);

            $dates = [];
            $affluence = [];
            $colors = ['#a4c5e1', '#da91c4', '#595f20', '#b7b03d', '#9aca5f', '#87d7d7', '#412b81', '#edeaca', '#d4d580', '#f2dff4', '#e6b3e2', '#6ba4ce', '#973fbe', '#9e85d6'];

            foreach ($daterange as $date) {

                $nb = $this->spotRepository->getNbPerson($ets, $date);

                array_push($dates, $date->format('d-m-Y'));
                array_push($affluence, $nb);
            }

            $form = $this->createForm(EtsType::class, $ets);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {

                $this->em->flush();
                //Une fois le flush() réalisé on peut recréer le QRCode avec la nouvelle photo
                $ets->createQRCode($ets);
                $this->em->flush();
                $this->addFlash('success', 'Votre image a bien été ajoutée');
                return $this->redirectToRoute('admin.ets.show', array('url' => $ets->getUrl()));
            }

            return $this->render('ets/show.html.twig', [
                'form' => $form->createView(),
                'ets' => $ets,
                'current_menu' => 'ets',
                'dates' => json_encode($dates),
                'affluence' => json_encode($affluence),
                'colors' => json_encode($colors),
                'admin' => $user,
                'button' => 'Ajouter'
            ]);
        }
    }

    /**
     * Fonction qui permet de créer un nouvel établissement
     * @Route("/ets/create", name="admin.ets.add")
     * @param Request $request
     * @return Response
     */
    public function add(Request $request): Response
    {
        $user = $this->security->getUser();
        if (!isset($user)) {
            $this->addFlash('error', 'Vous n\'avez pas les autorisations nécessaires');
            return $this->redirectToRoute('home');
        } elseif (isset($user) && ($user->getUserGroup() == 1 || $user->getUserGroup() == 5)) {

            $ets = new Ets();
            $ets->setOwnerId($user->getId());
            $ets->setStatus(1);


            $form = $this->createForm(EtsType::class, $ets);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $ets->createQRCode($ets);
                $date = new DateTime();
                $ets->setUpdatedAt($date);
                $this->em->persist($ets);
                $this->em->flush();
                $this->addFlash('success', 'Etablissement créé avec succès');
                return $this->redirectToRoute('ets.index');
            }

            return $this->render('ets/edit.html.twig', [
                'form' => $form->createView(),
                'ets' => $ets,
                'admin' => $user,
                'current_menu' => 'ets',
                'button' => 'Ajouter',
                'action' => 'add'
            ]);
        }
    }
    /**
     * Fonction qui permet de supprime un établissement
     * @Route("delete/{id}", name="admin.ets.delete")
     * @param Ets $ets
     * @return Response
     */
    public function delete(Ets $ets, Request $request): Response
    {
        if ($this->isCsrfTokenValid('delete' . $ets->getId(), $request->get('_token'))) {
            $request = $ets->deleteQRCode($ets->getQrcode());

            if ($request->type == 'error') {
                $this->addFlash('error', 'Code ' . $request->code . " - " . $request->message);
                return $this->redirectToRoute('ets.index', [
                    'request' => $request
                ]);
            } else {
                $this->em->remove($ets);
                $this->em->flush();
                $this->addFlash('success', 'Etablissement supprimé avec succès');
            }
        }

        return $this->redirectToRoute('ets.index');
    }
}
