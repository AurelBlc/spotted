<?php

namespace App\Controller;

use App\Entity\Ets;
use App\Entity\Spot;
use App\Repository\SpotRepository;
use DateInterval;
use DatePeriod;
use DateTime;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;

class SpotController extends AbstractController
{
    /**
     * @var SpotRepository
     */
    private $repository;
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(SpotRepository $repository, EntityManagerInterface $em, Security $security)
    {
        $this->repository = $repository;
        $this->em = $em;
        $this->security = $security;
    }

    /**
     * Appeler une seule fois, le tableau est ensuite alimenté en ajax
     * @Route("/listPerson/{url}", name="admin.person.show")
     * @param Ets $ets
     * @return Response
     */
    public function showListPerson(Ets $ets, Request $request): Response
    {
        $user = $this->security->getUser();
        $dateTime = new DateTime();
        $list = $this->repository->getListPerson($ets, $dateTime);

        //Création des dates pour le select
        $start = (new DateTime())->modify('+1 day');
        $end = (new DateTime)->modify('-14 days');
        $interval = new DateInterval('P1D');

        //Pour obtenir un array de date, il faut passer par la fonction DatePeriod
        $daterange = new DatePeriod($end, $interval, $start);
        $dates = [];

        foreach ($daterange as $date) {
            array_push($dates, $date->format('d-m-Y'));
        }

        $datesDESC = array_reverse($dates);

        //Si l'utilisateur n'est pas connecté => retour à l'accueil
        if (!isset($user)) {
            return $this->redirectToRoute('home');
        }

        return $this->render('person/show.html.twig', [
            'current_menu' => 'ets',
            'list' => $list,
            'ets' => $ets,
            'dates' => $datesDESC,
            'date_request' => $datesDESC[0],
            'admin' => $user,
        ]);
    }

    /**
     * Fonction AJAX
     * @Route("/showList/{url}/{date_request}",name="admin.person.showList")
     * @ParamConverter("date_request", options={"format": "d-m-Y"})
     * @return JsonResponse
     */
    public function ListPerson(Ets $ets, DateTime $date_request): JsonResponse
    {

        $user = $this->security->getUser();
        $list = $this->repository->getListPerson($ets, $date_request);

        //Création des dates pour le select
        $start = (new DateTime())->modify('+1 day');
        $end = (new DateTime)->modify('-14 days');
        $interval = new DateInterval('P1D');

        //Pour obtenir un array de date, il faut passer par la fonction DatePeriod
        $daterange = new DatePeriod($end, $interval, $start);
        $dates = [];

        foreach ($daterange as $date) {
            array_push($dates, $date->format('d-m-Y'));
        }

        //Si l'utilisateur n'est pas connecté, il n'a pas accès à la page
        if (!isset($user)) {
            return $this->redirectToRoute('home');
        }
        return $this->json([
            'message' => 'success',
            'list' => $list,
            'date_request' => $date_request,
        ], 200);
    }

    /**
     * Fonction AJAX
     * @Route("/addPerson/{url}/{date_request}",name="admin.add.person")
     * @ParamConverter("date_request", options={"format": "d-m-Y"})
     *
     * @param EntityManagerInterface $em
     * @param Ets $ets
     * @param DateTime $date_request
     * @return JsonResponse
     */
    public function addPersonLater(EntityManagerInterface $em, Ets $ets, DateTime $date_request): JsonResponse
    {

        var_dump($_POST);
        die;
        $user = $this->security->getUser();

        // if (empty($_POST)) {
        //     return $this->json([
        //         'message' => $_POST,
        //     ], 200);
        // } else {


        //     //Si l'utilisateur n'est pas connecté, il n'a pas accès à la page
        //     if (!isset($user)) {
        //         return $this->redirectToRoute('home');
        //     }
        //     return $this->json([
        //         'message' => 'success',
        //         'date_request' => $date_request,
        //         'post' => $_POST
        //     ], 200);
        // }
    }

    /**
     * @Route("/ets/addSpot/{url}",name="addSpot")
     *
     * @return Response
     */
    public function addSpot(EntityManagerInterface $em, Ets $ets): Response
    {

        for ($i = 0; $i < 15; $i++) {
            $spot = new Spot(new DateTime());
            $spot->setIdEts($ets->getId());
            $spot->setName("Test" . $i);
            $spot->setSurname("Test" . $i);
            $spot->setDateIn((new DateTime)->modify('-2 day'));
            $spot->setPhoneNumber("000000000" . $i);
            $spot->setHasAgreedCondition(true);

            $em->persist($spot);
            $em->flush();
        }
        return $this->redirectToRoute('ets.index');
    }
}
