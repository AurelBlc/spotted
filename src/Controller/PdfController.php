<?php

namespace App\Controller;

use DateTime;
use App\Entity\Ets;
use Spipu\Html2Pdf\Html2Pdf;
use App\Repository\SpotRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class PdfController extends AbstractController
{
    /**
     * @Route("/ets/createPdf/{url}", name="admin.qrcode.dl")
     * @param Ets $ets
     */
    public function createPdf(Ets $ets, EntityManagerInterface $em): Response

    {
        if (!$ets->getQrcode()) {
            $ets->createQRCode();
            $em->persist($ets);
            $em->flush();
        }
        ob_start();
?>

        <style>
            #title {
                text-decoration: underline;
                font-size: 35px;
                margin-left: 34%;
            }

            .no-break {
                page-break-inside: avoid;
                page-break-after: avoid;
                page-break-before: avoid;
            }

            td {
                margin: 0;
            }
        </style>
        <page backtop="20mm" backleft="0mm" backbottom="0mm" backright="0mm">
            <page_header>
                <div><img src="./images/wavesOpacityTop.png" alt="" style="width: 100%;"></div>
            </page_header>
            <div id="title">
                INFO COVID-19
            </div>
            <table style="width: 99%;border:none" cellspacing="4mm" cellpadding="0" class="no-break">
                <tr>
                    <td align="justify">
                        <div style="width: 50%; font-size: 20px;margin: auto; margin-top: 10px;">Pour la sécurité de tous, merci de prendre quelques instants pour vous enregistrer sur le cahier de rappel numérique.</div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="position: relative; margin-top: 50px;">
                            <div style="width: 50%; position: absolute; top: 0; left: 22%;">
                                <img src="./images/scan.png" alt="" style="width: 50%;">
                                <p style="width: 50%;"><small>Scannez le QR Code à l'aide de <br> votre smartphone</small></p>
                            </div>
                            <div style="width: 50%; position: absolute; top: 0; right: -3%;">
                                <img src="./images/form.png" alt="" style="width: 50%;">
                                <p style="width: 50%;"><small>Renseignez le formulaire</small></p>
                            </div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td style="position: relative;">
                        <div>
                            <img src="./images/qrcode/<?= $ets->getQrCode() ?>" alt="" style="width:25%; position:absolute; top:20%; left: 38%;">
                        </div>
                    </td>
                </tr>
                <tr>
                    <td align="justify">
                        <div style="width: 50%; font-size: 20px; position: absolute; top:65%; left: 25%;">
                            Nous nous engageons à ce que vos données restent confidentielles et soient divulguées uniquement aux services de santé compétents (ARS, CPAM), le cas échéant.
                        </div>
                        <div style="width: 50%; font-size: 20px; position: absolute; top:75%; left: 25%;">Vos données seront supprimées dans un délai de 14 jours.</div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="width: 50%; position: absolute; top:90%; left: 25%;">
                            <span style="font-size: 10px;">Powered By</span>
                            <img src="./images/snc-bgwhite.png" alt="" style="width: 15%;">
                        </div>
                    </td>
                </tr>
            </table>
            <page_footer class="no-break">
                <div>
                    <img src="./images/wavesOpacityBottom.png" alt="" style="width: 100%;">
                </div>
            </page_footer>
        </page>
        <?php


        $content = ob_get_clean();

        $html2pdf = new Html2Pdf('P', 'A4', 'fr', true, 'UTF-8', [5, 5, 5, 5]);
        $html2pdf->pdf->SetDisplayMode('fullpage');
        $html2pdf->setTestTdInOnePage(false);
        $html2pdf->writeHTML($content);

        $response = $html2pdf->output($ets->getSlug() . '.pdf');

        return $response;
    }

    /**
     * @Route("/ets/createPdf/{url}/{date_request}", name="admin.list.dl")
     * @ParamConverter("date_request", options={"format": "d-m-Y"})
     * @param Ets $ets
     */
    public function createListPdf(SpotRepository $spotRepository, Ets $ets, DateTime $date_request, Secret $secret): Response

    {
        //Récupération de la liste de personne pour l'établissement et la date demandée.
        $list = $spotRepository->getListPerson($ets, $date_request);

        $today = new DateTime('NOW');

        $diff_dateTime = date_diff($today, $date_request, true);

        $diff = (int)$diff_dateTime->format("%a");

        if ($diff > 14) {
            throw $this->createAccessDeniedException();
        } else {



            ob_start();
        ?>
            <page backtop="10mm" backleft="10mm" backbottom="10mm" backright="10mm">
                <style>
                    #body {
                        margin: 15px;
                    }

                    #info,
                    #liste {
                        width: 100%;
                    }

                    #logo {
                        width: 100%;
                        margin-top: 30px;
                    }

                    table {
                        width: 100%;
                    }

                    .table-td {
                        width: 25%;
                    }

                    .bandeau {
                        width: 100%;
                        background: #9da9b7;
                        margin-top: 10px;
                        margin-bottom: 10px;
                    }

                    .m-25 {
                        margin: 15px;
                    }
                </style>
                <page_header>
                    <div><img src="./images/wavesOpacityTop.png" alt="" style="width: 100%;"></div>
                </page_header>
                <div id="body">
                    <table id="logo">
                        <tr>
                            <td style="width: 30%;" align="center">
                                <img src="./images/snc-bgwhite.png" alt="" style="width: 100px;">
                            </td>
                            <?php
                            if ($ets->getFilename() != "empty") { ?>

                                <td style="width: 40%">
                                </td>
                                <td style="width: 30%;" align="center">
                                    <img src="./images/ets/<?= $ets->getFilename() ?>" alt="" style="width: 100px;">
                                </td>
                            <?php } else { ?>
                                <td style="width: 70%">
                                <?php } ?>
                        </tr>
                    </table>
                    <div class="m-25"></div>
                    <table>
                        <tr>
                            <td class="bandeau"></td>
                        </tr>
                    </table>
                    <div class="m-25"></div>
                    <table id="info">
                        <tr>
                            <td style="width: 50%;" align="center">Lieu : <?= $ets->getName() ?></td>
                            <td style="width: 50%;" align="center">Date : <?= $date_request->format("d-m-Y") ?></td>
                        </tr>
                        <tr>
                            <td style="width: 50%;" align="center">Adresse : <?= $ets->getAddress() ?></td>
                            <td style="width: 50%;" align="center"></td>
                        </tr>
                    </table>
                    <div class="m-25"></div>
                    <table>
                        <tr>
                            <td class="bandeau"></td>
                        </tr>
                    </table>
                    <div class="m-25"></div>
                    <table id="liste" align="center">
                        <tr style=" width: 100%">
                            <td style="width: 5%;" align="center">#</td>
                            <td style="width: 20%;" align="center">NOM</td>
                            <td style="width: 20%;" align="center">Prénom</td>
                            <td style="width: 20%;" align="center">N° tel</td>
                        </tr>
                        <?php foreach ($list as $index => $person) : ?>
                            <tr>
                                <td style="width: 5%;" align="center"><?= $index + 1 ?></td>
                                <td style="width: 20%;" align="center"><?=$secret->decrypt($person->getSurname()) ?></td>
                                <td style="width: 20%;" align="center"><?= $secret->decrypt($person->getName()) ?></td>
                                <td style="width: 20%;" align="center"><?= $secret->decrypt($person->getPhoneNumber()) ?></td>
                            </tr>
                        <?php endforeach ?>
                    </table>
                </div>
                <page_footer class="no-break">
                    <div>
                        <img src="./images/wavesOpacityBottom.png" alt="" style="width: 100%;">
                    </div>
                </page_footer>
            </page>
<?php
        }
        $content = ob_get_clean();

        $html2pdf = new Html2Pdf('P', 'A4', 'fr', 'UTF-8');
        $html2pdf->pdf->SetDisplayMode('fullpage');
        $html2pdf->writeHTML($content);

        $response = $html2pdf->output($ets->getSlug() . '.pdf');

        return $response;
    }
}
