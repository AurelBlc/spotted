<?php

namespace App\Controller;

use DateTime;
use App\Entity\Ets;
use App\Entity\Admin;
use App\Form\AdminType;
use App\Form\PasswordType;
use App\Entity\ChangePassword;
use App\Form\ResetPasswordType;
use App\Repository\EtsRepository;
use App\Repository\SpotRepository;
use App\Repository\AdminRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Notification\ContactNotification;
use PhpParser\Node\Expr\Cast\Object_;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AdminController extends AbstractController
{
    /**
     * @var EtsRepository
     */
    private $repository;
    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EtsRepository $repository, EntityManagerInterface $em, Security $security)
    {
        $this->repository = $repository;
        $this->em = $em;
        $this->security = $security;
    }

    /**
     * @Route("/create/user", name="user.create")
     * @return Response
     */
    public function create(Request $request, UserPasswordEncoderInterface $encoder, ContactNotification $contactNotification, AdminRepository $adminRepository): Response
    {
        $admin = new Admin();
        $form = $this->createForm(AdminType::class, $admin);
        $form->handleRequest($request);

        if ($this->isCsrfTokenValid('subscribe', $request->get('_token'))) {
            if ($form->isSubmitted() && $form->isValid()) {                
                if (!$adminRepository->checkAdmin($admin)) {

                    if ($admin->getIsCertified()) {

                        $admin->setUsername($admin->getEmail());
                        $admin->setPassword($encoder->encodePassword($admin, $admin->getPassword()));
                        $admin->setUpdatedAt(new DateTime());
                        $this->em->persist($admin);
                        $this->em->flush();

                        //Envoie d'un mail de confirmation de création de compte
                        $contactNotification->notifyAtCreation($admin);

                        $this->addFlash('success', 'Création de compte validée');
                        return $this->redirectToRoute('login');
                    } else {
                        $this->addFlash('error', 'Vous n\'avez pas validé toutes les conditions');
                        return $this->render('form/admin.html.twig', [
                            'form' => $form->createView(),
                            'admin' => $admin,
                            'current_menu' => 'ets',
                            'action' => 'create'
                        ]);
                    }
                } else {
                    $this->addFlash('error', 'Création de compte impossible : l\'email renseigné est déjà utilisé');
                    return $this->redirectToRoute('login');
                }
            }
        }

        return $this->render('form/admin.html.twig', [
            'form' => $form->createView(),
            'admin' => $admin,
            'current_menu' => 'ets',
            'action' => 'create'
        ]);
    }

    /**
     * @Route("/edit/user/{url}", name="user.edit")
     * @return Response
     */
    public function edit(Admin $admin, Request $request, UserPasswordEncoderInterface $encoder, ContactNotification $contactNotification): Response
    {
        $user = $this->security->getUser();
        $oldEmail = $user->getEmail();
        if (!isset($user)) {
            $this->addFlash('error', 'Vous n\'avez pas les autorisations nécessaires');
            return $this->redirectToRoute('home');
        } elseif (isset($user) && ($user->getUserGroup() == 1 || $user->getUserGroup() == 8 || $user->getUserGroup() == 5)) {
            $form = $this->createForm(AdminType::class, $admin);
            $form->handleRequest($request);


            if ($form->isSubmitted() && $form->isValid()) {

                $newEmail = $form->get('email')->getData();

                $message = 'Utilisateur modifié avec succès';

                if ($oldEmail !== $newEmail) {
                    $message = 'Email modifié avec succès, vous allez recevoir un mail de confirmation';
                    $contactNotification->emailModificationNotification($admin);
                }

                $admin->setPassword($admin->getPassword());
                $admin->setUpdatedAt(new DateTime());

                $this->em->flush();
                $this->addFlash('success', $message);
                return $this->redirectToRoute('user.edit', array(
                    "url" => $admin->getUrl()
                ));
            }

            return $this->render('form/admin.html.twig', [
                'form' => $form->createView(),
                'admin' => $admin,
                'current_menu' => 'ets',
                'button' => 'Modifier',
                'action' => 'edit'
            ]);
        }
    }

    /**
     * @Route("/pwd/{url}", name="ets.admin.renewpwd")
     * @return Response
     */
    public function renewPWD(Admin $admin, UserPasswordEncoderInterface $encoder, Request $request): Response
    {
        $user = $this->security->getUser();
        if (!isset($user)) {
            $this->addFlash('error', 'Vous n\'avez pas les autorisations nécessaires');
            return $this->redirectToRoute('home');
        } elseif (isset($user) && ($user->getUserGroup() == 1 || $user->getUserGroup() == 8 || $user->getUserGroup() == 5)) {
            $changePassword = new ChangePassword();
            $form = $this->createForm(ResetPasswordType::class, $changePassword);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {

                $newPassword = $form->get('newPassword')['first']->getData();
                
                $newEncodedPassword = $encoder->encodePassword($admin, $newPassword);
                $admin->setPassword($newEncodedPassword);
                $admin->setUpdatedAt(new DateTime());
                $this->em->flush();
                $this->addFlash('success', 'Votre mot de passe à bien été changé !');
                return $this->redirectToRoute('ets.index', [
                    'admin' => $admin
                ]);
            }
        }
        return $this->render('form/pwd.html.twig', [
            'form' => $form->createView(),
            'admin' => $admin,
            'current_menu' => 'ets',
            'action' => 'renewPWD',
        ]);
    }


    /**
     * @Route("/unsubscribe/{url}",name="admin.unsubscribe")
     * @param Admin $admin
     * @return Response
     */
    public function unsubsribe(Admin $admin, EtsRepository $etsRepository, SpotRepository $spotRepository, ContactNotification $contactNotification): Response
    {

        //Récupération des Ets associés au compte
        $salles = $etsRepository->findEtsByAdmin($admin);

        if ($salles) {
            foreach ($salles as $salle) {
                //Delete les QRCode associés
                $salle->deleteQRCode($salle);

                //Delete les salles
                $spots = $spotRepository->getListByEts($salle);
                foreach ($spots as $spot) {
                    $this->em->remove($spot);
                    $this->em->flush();
                }

                //Delete les ets associés
                $this->em->remove($salle);
                $this->em->flush();
            }
        }

        $this->container->get('security.token_storage')->setToken(null);

        //Delete l'admin
        $this->em->remove($admin);
        $this->em->flush();

        $this->addFlash('error', 'Votre compte a bien été supprimé');

        //Envoie d'un mail de confirmation de création de compte
        $contactNotification->notifyAtUnsubscription($admin);

        return $this->redirectToRoute('home');
    }
}
