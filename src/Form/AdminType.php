<?php

namespace App\Form;

use App\Entity\Admin;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;

class AdminType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $advert = $event->getData();
            $form = $event->getForm();

            //create
            if (!$advert || null === $advert->getId()) {
                $form->add(
                    'password',
                    RepeatedType::class,
                    [
                        'type' => PasswordType::class,
                        'first_options'  => ['label' => 'Mot de passe', 'required' => true],
                        'second_options' => ['label' => 'Confirmer le mot de passe', 'required' => true]
                    ]
                )
                    ->add(
                        'isCertified',
                        CheckboxType::class,
                        [
                            'label'    => 'En cochant cette case, vous certifiez que les données recueillies ne serviront pas à des fins commerciales',
                            'required' => true,
                        ]
                    );
            }
            //edit
            else {
                $form->add('ImageFile', FileType::class, [
                    'required' => false,
                    'label' => "Choose an image",
                    'attr' => [
                        'placeholder' => "Upload an image"
                    ]
                ]);
            }
        });
        $builder
            ->add('name', TextType::class, ['required' => true])
            ->add('surname', TextType::class, ['required' => true])
            ->add('email', EmailType::class, ['required' => true]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Admin::class,
            'translation_domain' => 'forms'
        ]);
    }
}
