<?php

namespace App\Form;

use App\Entity\Spot;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

class SpotType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name')
            ->add('surname')
            ->add('phone_number')
            ->add('hasAgreedCondition', CheckboxType::class, [
                'label'    => 'J\'accepte que mes données personnelles soient transmises aux autorités sanitaires (CPAM ou ARS) afin d\'être contacté seulement dans le cas où une personne ayant été infectée par le COVID ait fréquenté cet établissment le même jour que moi',
                'required' => true
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Spot::class,
            'translation_domain' => 'forms'
        ]);
    }
}
