<?php

namespace App\Form;

use App\Entity\Ets;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class EtsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $advert = $event->getData();
            $form = $event->getForm();

            //create
            if (!$advert || null === $advert->getId()) {
                $form
                    ->add('name')
                    ->add('address');
            }
            //edit
            else {
                $form->add('imageFile', FileType::class, [
                    'required' => false,
                    'label' => "Choose an image",
                    'attr' => [
                        'placeholder' => "Upload an image"
                    ]
                ]);
            }
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Ets::class,
            'translation_domain' => 'forms'
        ]);
    }
}
