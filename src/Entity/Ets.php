<?php

namespace App\Entity;

use DateTime;
use DateTimeInterface;
use Cocur\Slugify\Slugify;
use Endroid\QrCode\QrCode;
use Doctrine\ORM\Mapping as ORM;
use App\Repository\EtsRepository;
use Endroid\QrCode\LabelAlignment;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\HttpFoundation\File\UploadedFile;


/**
 * @ORM\Entity(repositoryClass=EtsRepository::class)
 * @Vich\Uploadable()
 */
class Ets
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $address;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $url;

    /**
     * @ORM\Column(type="integer", options={"default" : 0})
     */
    private $status;

    /**
     * @ORM\Column(type="integer")
     */
    private $owner_id;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=255)
     */
    private $filename;

    /**
     * @var File|null
     * @Assert\Image(mimeTypes={"image/png", "image/jpeg", "image/jpg"})
     * @Vich\UploadableField(mapping="ets_image", fileNameProperty="filename")
     */
    private $imageFile;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTimeInterface|null
     */
    private $updated_at;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $qrcode;

    const URL_PATTERN = "192.168.1.18:8000/form/";
    const PATH = "/../../public/images/qrcode/";

    public function __construct()
    {
        $this->url = $this->generateRandomString();
        $this->update_at = new DateTime();
        $this->filename = 'empty';
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSlug(): string
    {
        return (new Slugify())->slugify($this->name);
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getStatus(): ?int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getOwnerId(): ?int
    {
        return $this->owner_id;
    }

    public function setOwnerId(int $owner_id): self
    {
        $this->owner_id = $owner_id;

        return $this;
    }

    public function getUrlPattern(): string
    {
        return $this->url;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    public function setUpdatedAt(\DateTimeInterface $updated_at): self
    {
        $this->updated_at = $updated_at;

        return $this;
    }

    /**
     * Get the value of image_filename
     */
    public function getFilename(): ?string
    {
        return $this->filename;
    }

    /**
     * Set the value of image_filename
     * @param  string|null  $image_filename
     * @return  self
     */
    public function setFilename(?string $filename): self
    {
        $this->filename = $filename;
        return $this;
    }

    /**
     * Get the value of imageFile
     *
     * @return  File|null
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * Set the value of imageFile
     *
     * @param  File|null  $imageFile
     *
     * @return  self
     */
    public function setImageFile($imageFile)
    {
        $this->imageFile = $imageFile;
        // Only change the updated at if the file is really uploaded to avoid database updates.
        // This is needed when the file should be set when loading the entity.
        if ($imageFile !== null) {
            $this->updated_at = new \DateTime('now');
        }

        return $this;
    }

    public function getQrcode(): ?string
    {
        return $this->qrcode;
    }

    public function setQrcode(string $qrcode): self
    {
        $this->qrcode = $qrcode;

        return $this;
    }

    public function createQRCode(Ets $ets)
    {
        
        $oldQrCode = $ets->getQrCode();
        $namer = $this->generateRandomString();
        $this->setQrcode($namer . '.png');

        $qrCode = new QrCode(self::URL_PATTERN . $this->url);
        $qrCode->setSize(300);

        // Set advanced options
        $qrCode->setWriterByName('png');
        $qrCode->setMargin(10);
        $qrCode->setEncoding('UTF-8');
        $qrCode->setForegroundColor(['r' => 255, 'g' => 255, 'b' => 255]);
        $qrCode->setBackgroundColor(['r' => 244, 'g' => 177, 'b' => 61]);
        $qrCode->setValidateResult(false);
        $qrCode->setLabel($this->getName(), 16, null, LabelAlignment::CENTER);
        //dd(__DIR__);
        if ($this->getFilename() !== 'empty') {
            $qrCode->setLogoPath(__DIR__ . '/../../public/images/ets/' . $this->getFilename());
            $qrCode->setLogoSize(50);
        }
        $qrCode->setValidateResult(false);

        //Si il y a déjà un, QRCode, on le supprime avant d'écraser la donnée
        if (!is_null($oldQrCode)) {
            $this->deleteQRCode($oldQrCode);
        }
        $qrCode->writeFile(__DIR__ . self::PATH . $namer . ".png");
    }

    public function deleteQRCode(string $qrCodeName)
    {
        $response = [];
        $response = (object)$response;
        if (file_exists(__DIR__ . self::PATH . $qrCodeName)) {
            //Suppression du qrcode
            unlink(__DIR__ . self::PATH . $qrCodeName);
            $response->type = 'success';
        } else {
            $response->type = 'error';
            $response->message = "Suppression du QRCode impossible";
            $response->code = 100000;
        }

        return $response;
    }

    private function generateRandomString($length = 15): string
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
