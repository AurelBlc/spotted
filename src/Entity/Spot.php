<?php

namespace App\Entity;

use App\Repository\SpotRepository;
use DateTime;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=SpotRepository::class)
 */
class Spot
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_Ets;

    /**
     * @ORM\Column(type="datetime")
     */
    private $date_in;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $surname;

    /**
     * @ORM\Column(type="string", length=20)
     * @Assert\Regex(
     *       pattern = "/(?:(?:\+|00)33|0)\s*[1-9](?:[\s.-]*\d{2}){4}/"
     *   )
     */
    private $phone_number;


    /**
     * @ORM\Column(type="boolean")
     */
    private $hasAgreedCondition;

    /**
     * @ORM\Column(type="boolean", options={"default" : false})
     */
    private $isSetManualy;


    public function __construct()
    {
        
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIdEts(): ?int
    {
        return $this->id_Ets;
    }

    public function setIdEts(int $id_Ets): self
    {
        $this->id_Ets = $id_Ets;

        return $this;
    }

    public function getDateIn(): ?\DateTimeInterface
    {
        return $this->date_in;
    }

    public function setDateIn(\DateTimeInterface $date_in): self
    {
        $this->date_in = $date_in;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    public function getPhoneNumber(): ?string
    {
        return $this->phone_number;
    }

    public function setPhoneNumber(string $phone_number): self
    {
        $this->phone_number = $phone_number;

        return $this;
    }

    /**
     * Get the value of hasAgreedCondition
     */
    public function getHasAgreedCondition()
    {
        return $this->hasAgreedCondition;
    }

    /**
     * Set the value of hasAgreedCondition
     *
     * @return  self
     */
    public function setHasAgreedCondition($hasAgreedCondition)
    {
        $this->hasAgreedCondition = $hasAgreedCondition;

        return $this;
    }

    /**
     * Get the value of isSetManualy
     */
    public function getIsSetManualy()
    {
        return $this->isSetManualy;
    }

    /**
     * Set the value of isSetManualy
     *
     * @return  self
     */
    public function setIsSetManualy($isSetManualy)
    {
        $this->isSetManualy = $isSetManualy;

        return $this;
    }
}
