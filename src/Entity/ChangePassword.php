<?php

namespace App\Entity;

use Symfony\Component\Security\Core\Validator\Constraints as SecurityAssert;


class ChangePassword
{

    /**
     * @SecurityAssert\UserPassword(
     *     message = "L'ancien mot de passe ne correspond pas"
     * )
     */
    protected $oldPassword;

    protected $newPassword;

    function getOldPassword()
    {
        return $this->oldPassword;
    }

    function getNewPassword()
    {
        return $this->newPassword;
    }

    function setOldPassword($oldPassword)
    {
        $this->oldPassword = $oldPassword;
        return $this;
    }

    function setNewPassword($password)
    {
        $this->newPassword = $password;
        return $this;
    }
}
