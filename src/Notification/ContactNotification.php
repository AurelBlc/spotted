<?php

namespace App\Notification;

use App\Entity\Admin;
use Twig\Environment;
use App\Entity\Contact;
use phpDocumentor\Reflection\Types\Context;
use Symfony\Component\Mime\Email;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ContactNotification extends AbstractController
{
    /**
     * @var Environment
     */
    private $renderer;

    /**
     * @var MailerInterface
     */
    private $mailer;



    public function __construct(MailerInterface $mailer, Environment $renderer)
    {
        $this->mailer = $mailer;
        $this->renderer = $renderer;
    }

    public function notify(Contact $contact)
    {

        $email = (new TemplatedEmail())
            ->from('noreply@spot-n-chill.fr')
            ->to('contact@spot-n-chill.fr')
            ->subject('Demande de contact sur Spot\'n\'Chill')
            ->htmlTemplate('emails/contact.html.twig')
            ->context([
                'contact' => $contact
            ]);

        $this->mailer->send($email);
    }

    public function notifyAtCreation(Admin $admin)
    {
        $email = (new TemplatedEmail())
            ->from('noreply@spot-n-chill.fr')
            ->to($admin->getEmail())
            ->subject('Inscription sur Spot\'n\'Chill')
            ->htmlTemplate('emails/create.html.twig')
            ->context([
                'admin' => $admin
            ]);

        $this->mailer->send($email);
    }

    public function notifyAtUnsubscription(Admin $admin)
    {
        $email = (new TemplatedEmail())
            ->from('noreply@spot-n-chill.fr')
            ->to($admin->getEmail())
            ->subject('Désinscription sur Spot\'n\'Chill')
            ->htmlTemplate('emails/unsubscribe.html.twig');

        $this->mailer->send($email);
    }

    public function resetPwd($admin)
    {
        $email = (new TemplatedEmail())
            ->from('noreply@spot-n-chill.fr')
            ->to($admin->getEmail())
            ->subject('Renouvellement du mot de passe')
            ->htmlTemplate('emails/resetpwd.html.twig')
            ->context([
                'admin' => $admin
            ]);


        $this->mailer->send($email);
    }

    public function emailModificationNotification(Admin $admin)
    {
        $email = (new TemplatedEmail())
            ->from('noreply@spot-n-chill.fr')
            ->to($admin->getEmail())
            ->subject('Modification de votre email')
            ->htmlTemplate('emails/emailChange.html.twig')
            ->context([
                'admin' => $admin
            ]);

        $this->mailer->send($email);
    }
}
