<?php

namespace App\Repository;

use DateTime;
use App\Entity\Ets;
use App\Entity\Spot;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Spot|null find($id, $lockMode = null, $lockVersion = null)
 * @method Spot|null findOneBy(array $criteria, array $orderBy = null)
 * @method Spot[]    findAll()
 * @method Spot[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SpotRepository extends ServiceEntityRepository
{
    /**
     * Nombre de jours pendant lesquels les spot sont gardés en mémoire.
     */
    private const DAYS_BEFORE_REMOVAL = 14;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Spot::class);
    }

    /**
     * @return Spot[]
     */
    public function getListPerson(Ets $ets, DateTime $date): array
    {

        return $this->createQueryBuilder('sp')
            ->where('sp.id_Ets = :id')
            ->andWhere('sp.date_in LIKE :today')
            ->setParameter('id', $ets->getId())
            ->setParameter('today', '%' . $date->format('Y-m-d') . '%')
            ->getQuery()
            ->getResult();
    }

    public function getNbPerson(Ets $ets, DateTime $date): int
    {

        return $this->createQueryBuilder('sp')
            ->select('count(sp.id)')
            ->where('sp.id_Ets = :id')
            ->andWhere('sp.date_in LIKE :today')
            ->setParameter('id', $ets->getId())
            ->setParameter('today', '%' . $date->format('Y-m-d') . '%')
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function countOldSpots(): int
    {
        return $this->createQueryBuilder('sp')
            ->select('count(sp.id)')
            ->where('DATE_DIFF(:now, sp.date_in) > :daysNb')
            ->setParameter('now', (new DateTime())->format('Y-m-d'))
            ->setParameter('daysNb', self::DAYS_BEFORE_REMOVAL)
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function removeOldSpots()
    {
        return $this->createQueryBuilder('sp')
            ->delete()
            ->where('DATE_DIFF(:now, sp.date_in) > :daysNb')
            ->setParameter('now', (new DateTime())->format('Y-m-d'))
            ->setParameter('daysNb', self::DAYS_BEFORE_REMOVAL)
            ->getQuery()
            ->getSingleScalarResult();
    }
    /**
     * @return Spot[]
     */
    public function getListByEts(Ets $ets): array
    {
        return $this->createQueryBuilder('sp')
            ->where('sp.id_Ets = :id_ets')
            ->setParameter('id_ets', $ets->getId())
            ->getQuery()
            ->getResult();
    }
}
